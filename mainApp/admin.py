from django.contrib import admin
from mainApp.models import *

admin.site.register(Student)
admin.site.register(Staff)
admin.site.register(Notice)
admin.site.register(Class)
admin.site.register(Session)
admin.site.register(Subject)
admin.site.register(StudentClass)
admin.site.register(UserNotice)
admin.site.register(Class_w_Section)
admin.site.register(TeacherClass)
admin.site.register(ClassNotesTopic)
admin.site.register(ClassNote)
