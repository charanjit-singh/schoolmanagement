from mainApp.views_staff import *

# Create your views here.
def is_student(user):
    try:
        studentObj = Student.objects.get(auth_user = user)
        return True
    except Student.DoesNotExist:
        return False

# ---------------------------------------------------------------------------------------------
# --------------------------------- REST VIEWS  -----------------------------------------------
# ----------------------------------------------------------------------------------------------
def get_current_session():
    return Session.objects.filter(is_current = True)[0]
class StudentDetailUser(APIView):
    

    def get(self,request,format = None):
        session = str(Session.objects.filter(is_current = True)[0])
        print(Session.objects.filter(is_current=True)[0])
        print(Student.objects.get(auth_user = request.user).current_class)
        test = "test!!"
        try:
            student = Student.objects.get(auth_user = request.user)
        except:
            raise Http404
        serializer = StudentSerializer(student,context = {'request':request,'test':test})
        return Response({'user_detail':serializer.data,'current_session':session})
        

    def put(self, request, format=None):
        student = self.get_object()
        serializer = StudentSerializer(student, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, format=None):
        student = self.get_object(pk)
        student.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class SubjectListUser(APIView):
    #permission_classes = (permissions.IsAuthenticated, IsOwnerOrHasNotice)

    
    def get(self, request, format=None):
        #teacher = str(TeacherClass.objects.get(subject = Subject.objects.filter(_class = Student.objects.get(auth_user = request.user).current_class)[0]).teacher)
        user = request.user
        time = datetime.now()
        try:
    
            subjects = Subject.objects.filter(_class = Student.objects.get(auth_user = request.user).current_class)
        except:
            raise Http404
        paginator = PageNumberPagination()
        notices = paginator.paginate_queryset(subjects, request)
        serializer = SubjectSerializer(subjects, many=True, context={'request': request})
        return paginator.get_paginated_response(serializer.data)

    def post(self, request, format=None):
        # only allow staff users to post the data
        serializer = SubjectSerializer(data=request.data)
        if serializer.is_valid():
            subjects = serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class ClassNoteListUser(APIView):
    #permission_classes = (permissions.IsAuthenticated, IsOwnerOrHasNotice)

    
    def get(self, request, format=None):
        #teacher = str(TeacherClass.objects.get(subject = Subject.objects.filter(_class = Student.objects.get(auth_user = request.user).current_class)[0]).teacher)
        user = request.user
        time = datetime.now()
        try:
    
            notes = ClassNote.objects.filter(updated_by = request.user)
        except:
            raise Http404
        paginator = PageNumberPagination()
        notes = paginator.paginate_queryset(notes, request)
        serializer = ClassNoteSerializer(notes, many=True, context={'request': request})
        return paginator.get_paginated_response(serializer.data)

    def post(self, request, format=None):
        # only allow staff users to post the data
        serializer = SubjectSerializer(data=request.data)
        if serializer.is_valid():
            subjects = serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class Subject_Topic(APIView):
    def get(request,self,pk,format = None):
        try:
            topics = ClassNotesTopic.objects.filter(subject = Subject.objects.get(id = pk))
        except:
            raise Http404

        serializer = TopicSerializer(topics,many = True,context = {'request':request})
        return Response(serializer.data)
    def post(self, request, format=None):
        # only allow staff users to post the data
        serializer = TopicSerializer(data=request.data)
        if serializer.is_valid():
            subjects = serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class Subject_Topic_Notes(APIView):
    def get(request,self,pk,pk2,format = None):
        try:
            topics = ClassNotesTopic.objects.filter(subject = Subject.objects.get(id = pk),id = pk2)[0]
            notes = ClassNote.objects.filter(topic = topics)    
        except:
            raise Http404
        serializer = ClassNotesTopicSerializer(notes,many = True,context = {'request':request})
        return Response(serializer.data)
    def post(self, request, format=None):
        # only allow staff users to post the data
        serializer = ClassNotesTopicSerializer(data=request.data)
        if serializer.is_valid():
            subjects = serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



# ---------------------------------------------------------------------------------------------
# --------------------------------- HTML VIEWS  -----------------------------------------------
# ----------------------------------------------------------------------------------------------


def login_page(request):
    # TODO: If already logged in then redirect to /student/ or /staff/ depending upon the type of user.

    dictV = {}
    if request.method=="POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = auth.authenticate(username = username, password = password)
        print(username, password, user)
        if not user:
            dictV['error'] = "Invalid username and password combination."
            return render(request, 'login.html', dictV)
        auth.login(request, user)
        try:
            studentObj = Student.objects.get(auth_user = user)
            return HttpResponseRedirect('/student/')
        except Student.DoesNotExist:
            try:
                staffObj = Staff.objects.get(auth_user = user)
                return HttpResponseRedirect('/staff/')
            except Staff.DoesNotExist:
                pass
        
        dictV['error'] = 'Invalid Account Type'
        return render(request,'login.html',dictV)

    return render(request,'login.html',{})


#Logout
@login_required
def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/')


@login_required
@user_passes_test(is_student)
def student_dashboard(request):
    """Student's Dashboard"""
    return render(request,'students/dashboard.html',{})

@login_required
@user_passes_test(is_student)
def student_notices_list(request):
    """Student's Notices page"""
    return render(request,'students/notices.html',{})

@login_required
@user_passes_test(is_student)
def student_notice_details(request,pk):
    """Student's Notice Details"""
    return render(request,'students/notice_details.html',{})