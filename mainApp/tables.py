from mainApp.models import *
from table import Table
from table.columns import Column

class StudentTable(Table):
    admission_number = Column(field = 'admission_number')
    first_name = Column(field='first_name')
    last_name = Column(field='last_name')
    fathers_first_name = Column(field='fathers_first_name')
    fathers_last_name = Column(field='fathers_last_name')
    contact_number = Column(field='contact_number')
    
    class Meta:
        model = Student
