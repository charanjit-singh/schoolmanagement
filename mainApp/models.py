from django.db import models
from django.contrib.auth.models import User
from bs4 import BeautifulSoup
# Models Definition:


def get_current_session():
    return Session.objects.filter(is_current = True)[0]

class Student(models.Model):
    auth_user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='student')
    admission_number = models.CharField(max_length=50, unique=True)
    year_of_admission = models.CharField(blank=True, max_length=8,default="")
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64, blank=True)
    gender = models.CharField(blank=True, max_length=16,default="")
    adhaar_number = models.CharField(blank=True,max_length=64,default="")
    fathers_first_name = models.CharField(max_length=64, blank=True)
    fathers_last_name = models.CharField(max_length=64, blank=True)
    mothers_first_name = models.CharField(max_length=64, blank=True)
    mothers_last_name = models.CharField(max_length=64, blank=True)
    gaurdians_first_name = models.CharField(max_length=64, blank=True)
    gaurdians_last_name = models.CharField(max_length=64, blank=True)
    profile_pic = models.ImageField(upload_to='students/profilePics/', blank=True, null=True)
    contact_number = models.CharField(max_length=16, blank=True)
    emergency_contact_number = models.CharField(max_length=16, blank=True)
    blood_group = models.CharField(max_length=16, blank=True)
    medical_problems = models.TextField(blank=True)
    address = models.TextField(blank=True)

    def __str__(self):
        return str(self.auth_user.username)

    @property
    def current_class(self):
        current_session = get_current_session()
        return StudentClass.objects.get(student = self, session = current_session).class_w_section._class
    

    @property
    def current_section(self):
        current_session = get_current_session()
        return StudentClass.objects.get(student = self, session = current_session).class_w_section.section
    

class Staff(models.Model):
    TYPE_TEACHER = "Teacher"
    TYPE_STAFF = "Staff"
    TYPE_CHOICES = ((TYPE_TEACHER,TYPE_TEACHER),(TYPE_STAFF,TYPE_STAFF),)

    auth_user = models.OneToOneField(User, on_delete=models.CASCADE, related_name = 'staff')
    profile_pic = models.ImageField(upload_to='staff/profilePics/', blank=True, null=True)
    staff_type = models.CharField(choices=TYPE_CHOICES, max_length=50)
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64, blank=True)
    gender = models.CharField(max_length=64)
    contact_number = models.CharField(max_length=16, blank=True)
    address = models.TextField(blank=True)
    blood_group = models.CharField(max_length=16, blank=True)

    def __str__(self):
        return str(self.auth_user.username)


class Class(models.Model):
    name = models.CharField(max_length=64)

    def __str__(self):
        return str(self.name)

class Class_w_Section(models.Model):
    """Class with Section """
    _class = models.ForeignKey("mainApp.Class",  on_delete=models.CASCADE)
    section = models.CharField(max_length=8)

    def __str__(self):
        return str(self._class) + " [" + self.section + "]" 

    @property
    def class_teacher(self):
        try:
            teacher = TeacherClass.objects.filter(session = get_current_session(), class_w_section = self, is_class_teacher = True)[0].teacher
        except:
            return None
        return teacher
            

class Session(models.Model):
    period = models.CharField(max_length=64)
    is_current = models.BooleanField(default = False)

    # def save()
    # TODO: if no other session exists:
    #           Mark current session as is_current = True

    def __str__(self):
        return self.period
    

class StudentClass(models.Model):
    session = models.ForeignKey("mainApp.Session", on_delete=models.CASCADE)
    student = models.ForeignKey("mainApp.Student", on_delete=models.CASCADE)
    class_w_section = models.ForeignKey("mainApp.Class_w_Section",  on_delete=models.CASCADE)
    roll_number = models.PositiveIntegerField(blank=True)

    def __str__(self):
        return str(self.student) + " " + str(self.class_w_section) + " " + str(self.session) 

    # TODO: if session and student with same details already exists then update instead of adding or raise exception
    

class TeacherClass(models.Model):
    session = models.ForeignKey("mainApp.Session", on_delete=models.CASCADE)
    teacher = models.ForeignKey("mainApp.Staff", on_delete=models.CASCADE)
    subject = models.ForeignKey("mainApp.Subject", on_delete=models.CASCADE)
    class_w_section = models.ForeignKey("mainApp.Class_w_Section",  on_delete=models.CASCADE)
    is_class_teacher = models.BooleanField(default=False)
    
    def __str__(self):
        return str(self.teacher) + " teaching  " + str(self.subject) + " to Class " + str(self.class_w_section) + " Session: "+str(self.session) 


class Subject(models.Model):
    subject_code = models.CharField(max_length=64, unique=True)
    name = models.CharField(max_length=128)
    _class = models.ForeignKey("mainApp.Class",  on_delete=models.CASCADE)

    def __str__(self):
        return self.subject_code + " : "+  self.name 

    @property
    def topic_count(self):
        return self.classnotestopic_set.all().count()
    

class ClassNotesTopic(models.Model):
    subject = models.ForeignKey("mainApp.Subject", on_delete=models.PROTECT)
    name = models.CharField(default="Topic", max_length=128)
    description = models.TextField(default="", null=True, blank=True)
    updated_by = models.ForeignKey(User, on_delete = models.PROTECT)
    updated_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.subject) + ": " + self.name

    @property
    def notes_count(self):
        return self.classnote_set.all().count()
    
 
def notes_directory_path(note, filename):
    return 'class_notes/topic_{0}/{1}'.format(note.topic.id, filename)

class ClassNote(models.Model):
    topic = models.ForeignKey("mainApp.ClassNotesTopic", on_delete=models.PROTECT)
    title = models.CharField(default="", max_length=256,null=True, blank=True)
    description = models.TextField(default="",null=True, blank=True)
    file = models.FileField(upload_to= notes_directory_path,blank=True,null=True)
    updated_by = models.ForeignKey(User, on_delete = models.PROTECT)
    updated_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.topic) + ' ' + self.title

class Notice(models.Model):
    TYPE_FEES = 'fees'
    TYPE_IMPORTANT = 'important'
    TYPE_GENERAL = 'general'
    TYPE_CHOICES = ((TYPE_FEES,TYPE_FEES),(TYPE_IMPORTANT,TYPE_IMPORTANT),(TYPE_GENERAL,TYPE_GENERAL),)
    
    title = models.CharField(max_length=128)
    body = models.TextField()
    type_notice = models.CharField(choices=TYPE_CHOICES, max_length=32)
    posted_by = models.ForeignKey("mainApp.Staff", on_delete=models.CASCADE)
    publish_on = models.DateTimeField(auto_now=False, auto_now_add=False)
    description_slug = models.TextField(null = True, blank=True, default = "")

    def save(self, *args, **kwargs):
        soup = BeautifulSoup(self.body, 'html.parser')
        texts = soup.findAll(text=True)
        self.description_slug = u" ".join(t.strip() for t in texts)
        super(Notice, self).save( *args, **kwargs)
    
    def __str__(self):
        return "(" + self.type_notice + ") " + self.title 




class UserNotice(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    notice = models.ForeignKey("mainApp.Notice", on_delete=models.CASCADE)

    def __str__(self):
        return "User: "+ str(self.user) + " Notice: " + str(self.notice) 



    