from django.conf.urls import url, include
from mainApp import views

urlpatterns = [
# -----------------  COMMON HTML VIEWS ----------------------

    url(r'^login/$', views.login_page),
    url(r'^logout/$', views.logout),

# --------------  COMMON REST VIEWS ---------------------------

    url(r'api/notices/$',views.NoticesListUser.as_view()),
    url(r'api/notices/(?P<pk>[0-9]+)/$',views.NoticeDetailUser.as_view()),

# ------------------ STUDENT HTML VIEWS ------------------------

    url(r'^student/$', views.student_dashboard),
    url(r'^student/notices/$', views.student_notices_list),
    url(r'^student/notices/(?P<pk>[0-9]+)/$', views.student_notice_details),


# -------------------- STUDENT REST VIEWS ----------------------------

    url(r'api/student/user-detail/$',views.StudentDetailUser.as_view()),
    url(r'api/student/subjects/$',views.SubjectListUser.as_view()),
    url(r'api/student/subjects/(?P<pk>[0-9]+)/$',views.Subject_Topic.as_view()),
    url(r'api/student/subjects/(?P<pk>[0-9]+)/(?P<pk2>[0-9]+)/$',views.Subject_Topic_Notes.as_view()),

# --------------------- STAFF HTML VIEWS -------------------------
    url(r'^staff/$', views.staff_dashboard),
    url(r'^staff/notices/$', views.staff_notices),
    url(r'^staff/notices/(?P<pk>[0-9]+)/$', views.staff_details_notices),
    url(r'^staff/notices/(?P<pk>[0-9]+)/edit$', views.staff_edit_notices),
    url(r'^staff/notices/add/$', views.staff_add_notices),
    url(r'^staff/class-notes/$', views.staff_class_notes),
    url(r'^staff/class-notes/(?P<pk>[0-9]+)/$', views.staff_details_class_notes_topic),
    url(r'^staff/class-notes/showtopic/(?P<pk>[0-9]+)/$', views.staff_details_class_notes),

    url(r'^staff/sessions/$', views.staff_sessions),
    url(r'^staff/classes/$', views.staff_classes),
    url(r'^staff/classes/(?P<pk>[0-9]+)/$', views.staff_details_class),
    url(r'^staff/sections/(?P<pk>[0-9]+)/$', views.staff_details_section),
    url(r'^staff/subjects/$', views.staff_subjects),
    url(r'^staff/students/$', views.staff_students),
    url(r'^staff/students/(?P<pk>[0-9]+)/$', views.staff_details_student),
    url(r'^staff/students/update/(?P<pk>[0-9]+)/$', views.staff_update_student),
    url(r'^staff/teachers/$', views.staff_teachers),
    url(r'^staff/staff-members/$', views.staff_staff_members),
    url(r'^staff/staff-members/(?P<pk>[0-9]+)/$', views.staff_staff_member_detail),

# --------------------- STAFF REST VIEWS ---------------------------
    url(r'api/notices/(?P<pk>[0-9]+)/$', views.NoticeDetail.as_view()),
    url(r'api/notices/$', views.NoticesList.as_view()),
    url(r'api/students/$', views.StudentsList.as_view()),
    url(r'api/students/(?P<pk>[0-9]+)/$', views.StudentDetails.as_view(), name="student-detail"),
    #-----new----
    url(r'api/teacher/notes/$',views.ClassNoteListUser.as_view()),



]