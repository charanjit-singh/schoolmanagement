from mainApp.models import *

def get_my_notices(user):
    user_notices = UserNotice.objects.filter(user = user).values('notice')
    try:
        notices = Notice.objects.filter(id__in = user_notices)
    except:
        notices = Notice.objects.filter(id__in = user_notices)
        return notices

def get_current_session():
    return Session.objects.filter(is_current = True)[0]

def get_current_class_teacher(_class, _section):
	try:
		return TeacherClass.objects.filter(session = get_current_session(), class_w_section = Class_w_Section.objects.get(_class = _class, section = _section), is_class_teacher = True)[0].teacher
	except:
		return None

def get_class_students(_class):
	current_session = get_current_session()
	# get all sections of current class
	class_w_sections = Class_w_Section.objects.filter(_class = _class)
	class_students = StudentClass.objects.filter(session = current_session, class_w_section__in = class_w_sections)
	return class_students

def get_all_students():
	return Student.objects.all()
