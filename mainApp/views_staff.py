from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect, HttpResponse, Http404, JsonResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import auth
from django.utils.decorators import method_decorator
from rest_framework.decorators import api_view
from django.http import HttpResponseForbidden
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated  
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.exceptions import PermissionDenied
from django.conf import settings
from rest_framework import status
from mainApp.models import *
from mainApp.serializers import *
from mainApp.permissions import *
from datetime import *
from .forms import *
from .helperfunc import *
from .models import UserNotice

def is_staff(user):
    try:
        Staff.objects.get(auth_user = user)
        return True
    except Staff.DoesNotExist:
        return False

"""----------------------------------------------------------------------------------------
----------------------------- API VIEWS ----------------------------------------------------
-----------------------------------------------------------------------------------------"""
class StudentsList(APIView):
    """
    List all students
    """
    permission_classes = (permissions.IsAuthenticated, IsStaff)

    def get(self, request, format=None):
        paginator = PageNumberPagination()
        students = Student.objects.all().order_by('-pk')
        students = paginator.paginate_queryset(students, request)
        serializer = StudentSerializer(students, many=True, context={'request': request})
        return paginator.get_paginated_response(serializer.data)

class StudentDetails(APIView):
    """
    Details of student
    """
    permission_classes = (permissions.IsAuthenticated, IsStaff)

    def get_object(self, pk):
        try:
            return Student.objects.get(pk=pk)
        except Notice.DoesNotExist:
            raise Http404

    def get(self, request,pk, format=None):
        student = self.get_object(pk)
        serializer = StudentSerializer(student, context={'request': request})
        return Response(serializer.data)


class NoticesList(APIView):
    permission_classes = (permissions.IsAuthenticated, IsOwnerOrHasNotice)

    """
    List all notices, or create a new notice.
    """
    
    def get(self, request, format=None):
        user = request.user
        time = datetime.now()
        try:
            mStaff = user.staff
            notices = Notice.objects.filter(posted_by = mStaff).order_by('-pk')
        except:
            # Student Case
            user_notices = UserNotice.objects.filter(user = user).only('pk').values('notice')
            notices = Notice.objects.filter( publish_on__lte = time,  pk__in = user_notices).order_by('-pk')
        paginator = PageNumberPagination()
        notices = paginator.paginate_queryset(notices, request)
        serializer = NoticeSerializer(notices, many=True, context={'request': request})
        return paginator.get_paginated_response(serializer.data)

    def post(self, request, format=None):
        # only allow staff users to post the data
        serializer = NoticeSerializer(data=request.data)
        if serializer.is_valid():
            notice = serializer.save(posted_by = request.user.staff)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class NoticesListUser(APIView):
    permission_classes = (permissions.IsAuthenticated, IsOwnerOrHasNotice)

    """
    List all notices, or create a new notice.
    """
    
    def get(self, request, format=None):
        user = request.user
        time = datetime.now()
        user_notices = UserNotice.objects.filter(user = user).only('pk').values('notice')
        notices = Notice.objects.filter( publish_on__lte = time,  pk__in = user_notices).order_by('-pk')
        paginator = PageNumberPagination()
        notices = paginator.paginate_queryset(notices, request)
        serializer = NoticeSerializer(notices, many=True, context={'request': request})
        return paginator.get_paginated_response(serializer.data)

    def post(self, request, format=None):
        # only allow staff users to post the data
        serializer = NoticeSerializer(data=request.data)
        if serializer.is_valid():
            notice = serializer.save(posted_by = request.user.staff)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    

class NoticeDetail(APIView):
    """
    Retrieve, update or delete a notices instance.
    """
    permission_classes = (permissions.IsAuthenticated, IsOwnerOrHasNotice,)

    def get_object(self, pk):
        try:
            return Notice.objects.get(pk=pk)
        except Notice.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        notice = self.get_object(pk)
        try:
            staffUser = Staff.objects.get(auth_user = request.user)
            if not notice.posted_by == staffUser:
                raise PermissionDenied()
        except Staff.DoesNotExist:
            if not notice.usernotice_set.filter(user = request.user).count() > 0 :
                raise PermissionDenied()

        serializer = NoticeSerializer(notice, context={'request': request})
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        notice = self.get_object(pk)
        serializer = NoticeSerializer(notice, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        notice = self.get_object(pk)
        notice.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class NoticeDetailUser(APIView):

    def get(self,request,pk,format = None):
        try:
            notice = UserNotice.objects.get(pk=pk,user = request.user).notice
        except:
            raise Http404
        serializer = NoticeSerializer(notice,context = {'request':request})
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        notice = self.get_object(pk)
        serializer = NoticeSerializer(notice, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        notice = self.get_object(pk)
        notice.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class Subject_Topic(APIView):
    def get(request,self,pk,format = None):
        try:
            topics = ClassNotesTopic.objects.filter(subject = Subject.objects.get(id = pk))
        except:
            raise Http404

        serializer = TopicSerializer(topics,many = True,context = {'request':request})
        return Response(serializer.data)

class Subject_Topic_Notes(APIView):
    def get(request,self,pk,pk2,format = None):
        try:
            topics = ClassNotesTopic.objects.filter(subject = Subject.objects.get(id = pk),id = pk2)[0]
            notes = ClassNote.objects.filter(topic = topics)

            
        except:
            raise Http404
        serializer = ClassNotesTopicSerializer(notes,many = True,context = {'request':request})
        return Response(serializer.data)








"""----------------------------------------------------------------------------------------
----------------------------- HTML VIEWS ----------------------------------------------------
-----------------------------------------------------------------------------------------"""

@login_required
@user_passes_test(is_staff)
def staff_dashboard(request):
    """Staff's Dashboard View"""
    return render(request,'staff/dashboard.html',{})

@login_required
@user_passes_test(is_staff)
def staff_class_notes(request):
    """Staff's List Class Notes View"""
    dictV = {}
    dictV['classes'] = Class.objects.all().order_by('name')
    return render(request,'staff/list-class-notes.html',dictV)

@login_required
@user_passes_test(is_staff)
def staff_details_class_notes_topic(request, pk):
    """Staff's Details Class Notes View"""
    dictV = {}
    subject = get_object_or_404(Subject, pk = pk)
    classnotestopics = subject.classnotestopic_set.all().order_by('-updated_on')
    dictV['topics'] = classnotestopics
    dictV['subject'] = subject
    return render(request,'staff/details-class-notes.html',dictV)

@login_required
@user_passes_test(is_staff)
def staff_details_class_notes(request, pk):
    """Staff's Details Class Notes View"""
    dictV = {}
    topic = get_object_or_404(ClassNotesTopic, pk = pk)
    return render(request,'staff/details-class-notes.html',dictV)

@login_required
@user_passes_test(is_staff)
def staff_classes(request):
    """Staff's List Class View"""
    dictV = {}
    classes = Class.objects.all().order_by('-pk')
    dictV['classes'] = ClassSerializer(classes, many=True, context={'request':request}).data
    return render(request,'staff/list-classes.html', dictV )

@login_required
@user_passes_test(is_staff)
def staff_details_class(request, pk):
    """Staff's Details Class View"""
    dictV = {}
    _class = get_object_or_404(Class, pk = pk)
    dictV['class'] = ClassDetailsSerializer(_class, context={'request':request}).data
    dictV['class_students'] = get_class_students(_class).order_by('roll_number')
    return render(request,'staff/details-classes.html',dictV)

@login_required
@user_passes_test(is_staff)
def staff_details_section(request, pk):
    """Staff's Details Section View"""
    dictV = {}
    class_w_section = get_object_or_404(Class_w_Section, pk = pk)
    session = get_current_session()
    class_students  = StudentClass.objects.filter(session = session, class_w_section = class_w_section).order_by('roll_number')
    dictV['class_students'] = class_students
    dictV['class_w_section'] = class_w_section
    dictV['className'] = class_w_section._class
    dictV['subjects'] = Subject.objects.filter(_class = dictV['className'])
    return render(request,'staff/details-section.html',dictV)

@login_required
@user_passes_test(is_staff)
def staff_details_notices(request, pk):
    """Staff's Details Notices View"""
    dictV = {}
    notice = get_object_or_404(Notice, pk = pk)
    dictV['notice'] = notice
    return render(request,'staff/details-notice.html',dictV)

@login_required
@user_passes_test(is_staff)
def staff_edit_notices(request, pk):
    """Staff's Edit Notices View"""
    dictV = {}
    notice = get_object_or_404(Notice, pk = pk)
    dictV['notice'] = notice
    if request.method == "POST":
        action = request.POST.get('action')
        if action=="delete":
            notice.delete()
            return Response({'status':'success'})
        if action == "edit":
            notice_type = request.POST.get("type")
            notice_title = request.POST.get("title")
            notice_time = request.POST.get("time")
            notice_content = request.POST.get("content")
            notice.title = notice_title
            notice.type_notice = notice_type
            notice.body = notice_content
            notice.save()
            notice.publish_on = notice_time
            try:
                notice.save()
            except:
                pass
            dictV['notice'] = notice
            dictV['msgType'] = "success"
            dictV['message'] = "Notice Edited Successfully"
            return HttpResponseRedirect('/staff/notices/'+pk+'/')
    return render(request,'staff/edit-notices.html',dictV)


@login_required
@user_passes_test(is_staff)
def staff_notices(request):
    """List notices"""
    dictV = {}
    notice_type = request.GET.get('type')

    if not notice_type or notice_type=="all":
        notices = Notice.objects.all().order_by('-pk')
    else:
        notices = Notice.objects.filter(type_notice = notice_type).order_by('-pk')
    # TODO:  add get my notices instead of all notices
    dictV['allNoticesCount'] = Notice.objects.all().count()
    dictV['feesNoticesCount'] = Notice.objects.filter(type_notice = "fees").count()
    dictV['generalNoticesCount'] = Notice.objects.filter(type_notice = "general").count()
    dictV['impNoticesCount'] = Notice.objects.filter(type_notice = "important").count()
    dictV['notices'] = notices
    return render(request,'staff/list-notices.html',dictV)

@login_required
@user_passes_test(is_staff)
def staff_add_notices(request):
    dictV = {}
    if request.method == "POST":
        me_user = request.user
        me_staff = Staff.objects.get(auth_user = me_user)

        notice_type = request.POST.get("type")
        notice_title = request.POST.get("title")
        notice_users = request.POST.get("users")
        users = None
        if notice_users == "ALL_USERS":
            # All users
            users = User.objects.all()
        elif notice_users == "ALL_TEACHERS":
            # All teachers
            teachers = Staff.objects.filter(staff_type = "Teacher").values('auth_user')
            users = User.objects.filter(id__in = teachers)

        elif notice_users == "ALL_STUDENTS":
            students = Student.objects.all().values('auth_user')
            users = User.objects.filter(id__in = students)
            # All students
        elif notice_users == "ALL_STAFF_MEMBERS":
            # All staff members
            staff_users = Staff.objects.filter(staff_type = "Staff").values('auth_user')
            users = User.objects.filter(id__in = staff_users)

        elif notice_users == "SELECTED_USERS":
            pass
            # TODO: Selected Users
        notice_time = request.POST.get("time")
        notice_content = request.POST.get("content")
        if(not(notice_type and notice_title and notice_users and notice_time and notice_content)):
            dictV['msgType'] = "danger"
            dictV['message'] = "Please check the content!"
            return render(request,'staff/add-notices.html',dictV)
        notice = Notice.objects.create(
                                    title = notice_title,
                                    body = notice_content,
                                    type_notice = notice_type,
                                    posted_by = me_staff,
                                    publish_on = notice_time
                                 )
        # create  usernotices 
        user_notices = [ UserNotice(notice = notice, user = user) for user in users ]
        UserNotice.objects.bulk_create(user_notices) 
        
        dictV['msgType'] = "success"
        dictV['message'] = "Notice scheduled on " + notice_time + " for " + str(users.count()) + " users !!"
        
    return render(request,'staff/add-notices.html',dictV)

@login_required
@user_passes_test(is_staff)
def staff_sessions(request):
    dictV = {}
    dictV['sessions'] = Session.objects.all().order_by('-pk')
    return render(request,'staff/list-sessions.html',dictV)

@login_required
@user_passes_test(is_staff)
def staff_students(request):
    dictV = {}
    dictV['students'] = get_all_students().order_by('-pk')
    return render(request,'staff/list-students.html',dictV)

@login_required
@user_passes_test(is_staff)
def staff_details_student(request,pk):
    dictV = {}
    student = get_object_or_404(Student, pk = pk)
    dictV['student'] = student
    return render(request,'staff/details-student.html',dictV)

@api_view
@login_required
@user_passes_test(is_staff)
def staff_update_student(request, pk):
    dictV = {}
    student = get_object_or_404(Student, pk = pk)
    dictV['student'] = student
    if request.method=="POST":
        serializer = StudentSerializer(student, data = request.data)
        if serializer.is_valid():
            serializer.save()
            print(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    return Response({"METHOD":"GET NOT ALLOWED"})


@login_required
@user_passes_test(is_staff)
def staff_subjects(request):
    return render(request,'staff/list-subjects.html',{})

@login_required
@user_passes_test(is_staff)
def staff_teachers(request):
    dictV = {}
    dictV['teachers'] = Staff.objects.filter(staff_type = "Teacher").order_by('-pk')
    return render(request,'staff/list-teachers.html',dictV)

@login_required
@user_passes_test(is_staff)
def staff_staff_members(request):
    dictV = {}
    dictV['staff_members'] = Staff.objects.filter(staff_type = "Staff").order_by('-pk')
    return render(request,'staff/list-staff-members.html',dictV)

@login_required
@user_passes_test(is_staff)
def staff_staff_member_detail(request,pk):
    dictV = {}
    staff_member = get_object_or_404(Staff, pk=pk)
    dictV['form'] = StaffMemberForm(instance = staff_member)
    if request.method=="POST":
        form = StaffMemberForm(request.POST, instance=staff_member)
        form.save()
        print(form.errors)
        dictV['form'] = form
    return render(request,'staff/details-staff-members.html',dictV)
