from rest_framework import permissions
from django.shortcuts import get_object_or_404
from mainApp.models import *

# Allow only admins that created notice to edit/delete  the notice, and read only to other users
# to whom the notice has been sent
class IsOwnerOrHasNotice(permissions.BasePermission):
    message = "You do not have access to this object."

    def has_permission(self, request, view):
        try:
            request.user.staff
            return True
        except:
            return request.method in permissions.SAFE_METHODS
        return False

    
    def has_object_permission(self, request, view, notice):
        if not request.user.is_authenticated:
            return False
        # notice 
        user = request.user
        try:
            staffUser = Staff.objects.get(auth_user = user)
            # check if staff user created this notice
            return notice.posted_by == staffUser

        except Staff.DoesNotExist:
            # that means user is not staff user
            pass
            
        return user in notice.usernotice_set.all()

class IsStaff(permissions.BasePermission):
    message = "You do not have access to this object."

    def has_permission(self, request, view):
        try:
            request.user.staff
            return True
        except:
            return False
