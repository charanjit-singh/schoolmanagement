from django.contrib.auth.models import User, Group
from rest_framework import serializers
from mainApp.models import *

class NoticeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Notice
        fields = ('pk','title', 'type_notice', 'body', 'publish_on', 'description_slug')

class StudentSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Student
        fields = "__all__"
        
class TeacherSerializer(serializers.ModelSerializer):
    class Meta:
        model = TeacherClass
        fields = (
            'teacher',
        )
 
class ClassSerializer(serializers.ModelSerializer):
    section_count = serializers.SerializerMethodField()
    subject_count = serializers.SerializerMethodField()
    class Meta:
        model = Class
        fields = ('id', 'name', 'section_count', 'subject_count')

    def get_section_count(self, obj):
        return Class_w_Section.objects.filter(_class = obj).count()

    def get_subject_count(self, obj):
        return Subject.objects.filter(_class = obj).count()

 
class ClassDetailsSerializer(serializers.ModelSerializer):
    sections = serializers.SerializerMethodField()
    subjects = serializers.SerializerMethodField()
    class Meta:
        model = Class
        fields = ('id', 'name', 'sections', 'subjects')

    def get_sections(self, obj):
        return Class_w_Section.objects.filter(_class = obj)

    def get_subjects(self, obj):
        return Subject.objects.filter(_class = obj)

class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subject
        fields = "__all__"

class ClassNoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClassNote
        fields = "__all__"
    
class TopicSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClassNotesTopic
        fields = "__all__"

class ClassNotesTopicSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClassNote
        fields = "__all__"