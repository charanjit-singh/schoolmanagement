# Generated by Django 2.1.5 on 2019-01-19 14:59

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Class',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64)),
            ],
        ),
        migrations.CreateModel(
            name='Notice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=128)),
                ('body', models.TextField()),
                ('type_notice', models.CharField(choices=[('fees', 'fees'), ('important', 'important'), ('general', 'general')], max_length=32)),
            ],
        ),
        migrations.CreateModel(
            name='Session',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('period', models.CharField(max_length=64)),
            ],
        ),
        migrations.CreateModel(
            name='Staff',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('staff_type', models.CharField(choices=[('Teacher', 'Teacher'), ('Staff', 'Staff')], max_length=50)),
                ('first_name', models.CharField(max_length=64)),
                ('last_name', models.CharField(max_length=64)),
                ('contact_number', models.CharField(max_length=16)),
                ('address', models.TextField()),
                ('blood_group', models.CharField(max_length=16)),
                ('auth_user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('admission_number', models.CharField(max_length=50, unique=True)),
                ('first_name', models.CharField(max_length=64)),
                ('last_name', models.CharField(max_length=64)),
                ('fathers_first_name', models.CharField(max_length=64)),
                ('fathers_last_name', models.CharField(max_length=64)),
                ('mothers_first_name', models.CharField(max_length=64)),
                ('mothers_last_name', models.CharField(max_length=64)),
                ('gaurdians_first_name', models.CharField(max_length=64)),
                ('gaurdians_last_name', models.CharField(max_length=64)),
                ('profile_pic', models.ImageField(blank=True, null=True, upload_to='students/profilePics/')),
                ('contact_number', models.CharField(max_length=16)),
                ('emergency_contact_number', models.CharField(max_length=16)),
                ('blood_group', models.CharField(max_length=16)),
                ('medical_problems', models.TextField(blank=True)),
                ('address', models.TextField()),
                ('auth_user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='StudentClass',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('section', models.CharField(max_length=2)),
                ('_class', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mainApp.Class')),
                ('session', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mainApp.Session')),
                ('student', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mainApp.Student')),
            ],
        ),
        migrations.CreateModel(
            name='StudentNotices',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('notice', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mainApp.Notice')),
                ('student', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mainApp.Student')),
            ],
        ),
        migrations.CreateModel(
            name='Subject',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('subject_code', models.CharField(max_length=64, unique=True)),
                ('name', models.CharField(max_length=128)),
                ('_class', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mainApp.Class')),
            ],
        ),
    ]
