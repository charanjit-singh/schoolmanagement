from .models import *
from django import forms

class StaffMemberForm(forms.ModelForm):
	class Meta:
		model = Staff
		exclude = ['auth_user',]