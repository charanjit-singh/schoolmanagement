# Generated by Django 2.1.5 on 2019-02-09 12:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainApp', '0008_teacherclass'),
    ]

    operations = [
        migrations.AddField(
            model_name='notice',
            name='description_slug',
            field=models.TextField(blank=True, default='', null=True),
        ),
        migrations.AddField(
            model_name='studentclass',
            name='roll_number',
            field=models.PositiveIntegerField(blank=True, default='0'),
            preserve_default=False,
        ),
    ]
