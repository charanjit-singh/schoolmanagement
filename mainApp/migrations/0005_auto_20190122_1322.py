# Generated by Django 2.1.5 on 2019-01-22 13:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mainApp', '0004_auto_20190122_1318'),
    ]

    operations = [
        migrations.RenameField(
            model_name='class_w_section',
            old_name='secction',
            new_name='section',
        ),
    ]
